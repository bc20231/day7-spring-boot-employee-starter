package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployeesByCompanyID(Long companyID) {
        return employeeRepository.findByCompanyID(companyID);
    }

    public void addEmployeesWithCompany(List<Employee> employees, Long companyID) {
        employees.stream().forEach(employee -> {
            employee.setCompanyID(companyID);
            employeeRepository.addEmployee(employee);
        });
    }
}