package com.thoughtworks.springbootemployee;

import java.util.List;

public class AddCompanyRequest {
    private String name;
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }
}
