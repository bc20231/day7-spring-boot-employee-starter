package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private List<Company> companies;

    public CompanyRepository() {
        this.companies = new ArrayList<>();
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findByID(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Company> getByPage(int pageNumber, int pageSize) {
        return companies.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company addCompany(String name) {
        Long id = generateID();
        Company newCompany = new Company(id, name);
        companies.add(newCompany);
        return newCompany;
    }

    private Long generateID() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }

    public Company updateCompany(Long id, Company company) {
        Company targetCompany = findByID(id);
        targetCompany.setName(company.getName());
        return  targetCompany;
    }

    public void deleteCompany(Long id) {
        Company targetCompany = findByID(id);
        companies.remove(targetCompany);
    }
}
