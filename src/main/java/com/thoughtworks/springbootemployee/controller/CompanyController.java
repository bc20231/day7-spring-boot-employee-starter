package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.AddCompanyRequest;
import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.EmployeeService;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final EmployeeService employeeService;

    public CompanyController(CompanyRepository companyRepository, EmployeeService employeeService) {
        this.companyRepository = companyRepository;
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Company getCompanyByID(@PathVariable Long id) {
        return companyRepository.findByID(id);
    }

    @GetMapping(path = "/{companyID}/employees")
    public List<Employee> getEmployeesByCompanyID(@PathVariable Long companyID) {
        return employeeService.getEmployeesByCompanyID(companyID);
    }

    @GetMapping(params = {"page", "pageSize"})
    public List<Company> getByPage(@RequestParam int page, @RequestParam int pageSize) {
        return companyRepository.getByPage(page, pageSize);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody AddCompanyRequest addCompanyRequest) {
        Company newCompany = companyRepository.addCompany(addCompanyRequest.getName());
        employeeService.addEmployeesWithCompany(addCompanyRequest.getEmployees(), newCompany.getId());
        return newCompany;
    }

    @PutMapping(path = "/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyRepository.deleteCompany(id);
    }
}
