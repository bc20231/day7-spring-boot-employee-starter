package com.thoughtworks.springbootemployee;

public class Employee {
    private Long id;
    private final String name;
    private Integer age;
    private final String gender;
    private Integer salary;
    private Long companyID;

    public Employee(Long id, String name, Integer age, String gender, Integer salary, Long companyID) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
        this.companyID = companyID;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public Long getCompanyID() {
        return companyID;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void setCompanyID(Long companyID) {
        this.companyID = companyID;
    }
}
